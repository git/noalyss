<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu
require_once '../include/constant.php';

require_once NOALYSS_INCLUDE.'/lib/ac_common.php';
MaintenanceMode("block.html");

/*! \file
 * \brief Login page
 */


// Verif if User and Pass match DB
    // if no, then redirect to the login page
$rep=new Database();

if (defined('MULTI') && MULTI == 0)
		$version = $rep->get_value('select val from repo_version');
	else
		$version = $rep->get_value('select val from version');

$http=new HttpInput();

if (  isset ($_POST["p_user"] ) )
{
    $http=new HttpInput();
    
    // clean OLD session 
    Noalyss_user::clean_session($http->post("p_user"));
    $User=new Noalyss_user($rep);
    $User->Check(false,'LOGIN');


    /*
     * Check repository version
     */

    if ($version != DBVERSIONREPO)
    {
        echo html_page_start();
        echo h1(_("Version base de donneés incorrecte"));
        echo span(_('Un instant svp'));
        echo alert(_('Version de base de données incorrectes, vous devez mettre à jour'));
        echo "<META HTTP-EQUIV=\"REFRESH\" content=\"3;url=admin-noalyss.php??action=upgrade&sb=database\">";
        exit();
    }
    if (defined('NOALYSS_CAPTCHA') && NOALYSS_CAPTCHA==true) 
    {
          include("securimage/securimage.php");
          $img = new Securimage();
          $valid = $img->check($_POST['captcha_code']);
          if ( $valid == false )
          {
          echo alert(_('Code invalide'));
          echo "<META HTTP-EQUIV=\"REFRESH\" content=\"0;url=index.php\">";
          exit();
        }
      }
      if ($User->get_access_mode()=='PC')
      {
        // force the nocache
        $backurl='user_login.php?v='.microtime(true);
        if ( isset ($_POST['backurl'])) {
              $backurl=urldecode($_POST['backurl']);
          }
        echo "<META HTTP-EQUIV=\"REFRESH\" content=\"0;url={$backurl}\">";
        exit();
      } else {
           echo "<META HTTP-EQUIV=\"REFRESH\" content=\"0;url=mobile.php\">";
           exit();
      }
}
else
{
    $rep=new Database();

    /*
     * Check repository version
     */

    if ( $version != DBVERSIONREPO)
    {
          echo html_page_start();
          echo h1(_("Version base de donneés incorrecte"));
          echo span(_('Un instant svp'));
          echo alert(_('Version de base de données incorrectes, vous devez mettre à jour'));
	    echo "<META HTTP-EQUIV=\"REFRESH\" content=\"3;url=admin-noalyss.php?action=upgrade&sb=database\">";
	    exit();

      }

    $User=new Noalyss_user($rep);
    $User->Check();

    echo "<META HTTP-EQUIV=\"REFRESH\" content=\"0;url=user_login.php?v=".microtime(true)."\">";
}
html_page_stop();
?>
