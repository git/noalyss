<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu
/*! \file
 * \brief Search module
 */
define('ALLOWED',TRUE);
require_once '../include/constant.php';
require_once NOALYSS_INCLUDE.'/class/dossier.class.php';
include_once NOALYSS_INCLUDE.'/lib/ac_common.php';
require_once NOALYSS_INCLUDE.'/class/acc_ledger.class.php';
require_once NOALYSS_INCLUDE.'/class/acc_ledger_search.class.php';
require_once NOALYSS_INCLUDE.'/lib/icon_action.class.php';
require_once NOALYSS_INCLUDE.'/lib/http_input.class.php';
MaintenanceMode("block.html");

if ( ! isset ($_SESSION[SESSION_KEY.'g_theme']))
{
    echo "<h2>"._('Vous  êtes déconnecté')."</h2>";
    die();
}

html_page_start($_SESSION[SESSION_KEY.'g_theme']);

load_all_script();

$http=new HttpInput();

$gDossier=dossier::id();

require_once NOALYSS_INCLUDE.'/class/database.class.php';
/* Admin. Dossier */

$cn=Dossier::connect();
include_once NOALYSS_INCLUDE . '/class/noalyss_user.class.php';

global $g_user;
$g_user=new Noalyss_user($cn);
$g_user->Check();
$act=$g_user->check_dossier($gDossier);
// AC CODE = SEARCH
if ($act =='P')
{
    redirect("extension.php?".dossier::get(),0);
    exit();
}
if ( $act=='X')
  {
     alert(_('Accès interdit'));
     exit();
  }
// display a search box
?>
<script>
/**
 * All the onload must be here otherwise the other will overwritten
 * @returns {undefined}
 */
window.onload=function ()
{
    create_anchor_up();
    init_scroll();
    sorttable.init();
}
</script>
<?php
$ledger=new Acc_Ledger_Search('ALL');
$search_box=$ledger->search_form();
echo '<div class="content">';

echo '<form method="GET">';
echo $search_box;
echo HtmlInput::submit("viewsearch",_("Recherche"));
 $button_search=new IButton("button", _('Filtre'));
$button_search->javascript=$ledger->build_search_filter();
echo $button_search->input();
?>
<input type="button" class="smallbutton" onclick="window.close()" value="<?php echo _('Fermer')?>">

<?php
echo '</form>';

//-----------------------------------------------------
// Display search result
//-----------------------------------------------------
if ( isset ($_GET['viewsearch']))
{

    // Navigation bar
    $step=$_SESSION[SESSION_KEY.'g_pagesize'];
    $page=$http->get("page","number",1);
    $offset=$http->get("offset","number",0);
    
    if (count ($_GET) == 0)
        $array=null;
    else
        $array=$_GET;
    $array['p_action']='ALL';
    list($sql,$where)=$ledger->build_search_sql($array);
    // Count nb of line
    $max_line=$cn->count_sql($sql);

    list($count,$a)=$ledger->list_operation($sql,$offset,0);
    $bar=navigation_bar($offset,$max_line,$step,$page);

    echo $bar;
    echo $a;
    echo $bar;
    /*
     * Export to csv
     */
    $r=HtmlInput::get_to_hidden(array('l','date_start','date_end','desc','amount_min','amount_max',
        'qcode','accounting','unpaid','gDossier','ledger_type','tag_option'));
    if (isset($_GET['r_jrn'])) {
        $http=new HttpInput();
        $a_rjrn=$http->get("r_jrn","array");
      foreach ($a_rjrn as $k=>$v) {
          // Protect : check that $k and $v are numeric
        if (isNumber($k)&&isNumber($v)) {
            $r.=HtmlInput::hidden('r_jrn['.$k.']',$v);
        }
      }
    }
    if (isset($_GET['tag'])) {
        $http=new HttpInput();
        $aTag=$http->get("tag","array");
      foreach ($aTag as $k=>$v) {
          // Protect : check that $k and $v are numeric
        if (isNumber($k)&&isNumber($v)) {
            $r.=HtmlInput::hidden('tag[]',$v);
        }
      }
    }
    echo '<form action="export.php" method="get">';
    echo $r;
    echo HtmlInput::hidden('act','CSV:histo');
    echo HtmlInput::submit('viewsearch',_('Export vers CSV'));
    echo HtmlInput::hidden('p_action','ALL');
    ?>
    <input type="button" class="smallbutton" onclick="window.close()" value="<?php echo _('Fermer')?>">

<?php
    echo '</form>';
}
echo '</div>';
?>
