<?php
$a_result = 
array (
  0 => 
  array (
    'jrn_def_name' => 'Achat',
    'tva_label' => '0%',
    'tva_rate' => '0.0000',
    'tva_both_side' => '0',
    'qp_vat_code' => '4',
    'amount_vat' => '0.0000',
    'amount_wovat' => '658.2500',
    'amount_sided' => '0.0000',
    'amount_noded_amount' => '0.0000',
    'amount_noded_tax' => '0.0000',
    'amount_noded_return' => '0.0000',
    'amount_private' => '0.0000',
  ),
  1 => 
  array (
    'jrn_def_name' => 'Achat',
    'tva_label' => '21%',
    'tva_rate' => '0.2100',
    'tva_both_side' => '0',
    'qp_vat_code' => '1',
    'amount_vat' => '38.4300',
    'amount_wovat' => '183.0000',
    'amount_sided' => '0.0000',
    'amount_noded_amount' => '0.0000',
    'amount_noded_tax' => '0.0000',
    'amount_noded_return' => '0.0000',
    'amount_private' => '0.0000',
  ),
  2 => 
  array (
    'jrn_def_name' => 'Frais Divers',
    'tva_label' => '21%',
    'tva_rate' => '0.2100',
    'tva_both_side' => '0',
    'qp_vat_code' => '1',
    'amount_vat' => '140.0400',
    'amount_wovat' => '666.8600',
    'amount_sided' => '0.0000',
    'amount_noded_amount' => '0.0000',
    'amount_noded_tax' => '0.0000',
    'amount_noded_return' => '0.0000',
    'amount_private' => '0.0000',
  ),
)
;
