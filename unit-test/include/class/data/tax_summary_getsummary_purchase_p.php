<?php
$a_result = array (
  0 => 
  array (
    'tva_label' => '0%',
    'tva_rate' => '0.0000',
    'tva_both_side' => '0',
    'qp_vat_code' => '4',
    'amount_vat' => '0.0000',
    'amount_wovat' => '658.2500',
    'amount_sided' => '0.0000',
    'amount_noded_amount' => '0.0000',
    'amount_noded_tax' => '0.0000',
    'amount_noded_return' => '0.0000',
    'amount_private' => '0.0000',
  ),
  1 => 
  array (
    'tva_label' => '21%',
    'tva_rate' => '0.2100',
    'tva_both_side' => '0',
    'qp_vat_code' => '1',
    'amount_vat' => '178.4700',
    'amount_wovat' => '849.8600',
    'amount_sided' => '0.0000',
    'amount_noded_amount' => '0.0000',
    'amount_noded_tax' => '0.0000',
    'amount_noded_return' => '0.0000',
    'amount_private' => '0.0000',
  ),
);
