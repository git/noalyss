<?php
$a_result = 
array (
  0 => 
  array (
    'jrn_def_name' => 'Vente',
    'tva_label' => '21%',
    'qs_vat_code' => '1',
    'tva_rate' => '0.2100',
    'tva_both_side' => '0',
    'amount_vat' => '219.5700',
    'amount_wovat' => '1045.6000',
    'amount_sided' => '0.0000',
    'tva_type' => 'O',
  ),
  1 => 
  array (
    'jrn_def_name' => 'Vente',
    'tva_label' => 'EXPORT',
    'qs_vat_code' => '6',
    'tva_rate' => '0.0000',
    'tva_both_side' => '0',
    'amount_vat' => '0.0000',
    'amount_wovat' => '1047.9000',
    'amount_sided' => '0.0000',
    'tva_type' => 'O',
  ),
  2 => 
  array (
    'jrn_def_name' => 'Vente',
    'tva_label' => 'INTRA',
    'qs_vat_code' => '5',
    'tva_rate' => '0.2100',
    'tva_both_side' => '1',
    'amount_vat' => '0.0000',
    'amount_wovat' => '1800.0000',
    'amount_sided' => '0.0000',
    'tva_type' => 'O',
  ),
  3 => 
  array (
    'jrn_def_name' => 'Vente',
    'tva_label' => 'VOIT',
    'qs_vat_code' => '1003',
    'tva_rate' => '0.2100',
    'tva_both_side' => '0',
    'amount_vat' => '19.1500',
    'amount_wovat' => '91.2000',
    'amount_sided' => '0.0000',
    'tva_type' => 'O',
  ),
  4 => 
  array (
    'jrn_def_name' => 'Vente différée',
    'tva_label' => 'EXPORT',
    'qs_vat_code' => '6',
    'tva_rate' => '0.0000',
    'tva_both_side' => '0',
    'amount_vat' => '0.0000',
    'amount_wovat' => '150.5600',
    'amount_sided' => '0.0000',
    'tva_type' => 'O',
  ),
);
