<?php

/**
 * Autogenerated file 
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 * class_operation_currency_sql.php
 *
 * @file
 * @brief abstract of the table public.operation_currency 
 */


 /**
 * @class Operation_currency_SQL
 * @brief ORM abstract of the table public.operation_currency 
 */

class Operation_currency_SQL extends Table_Data_SQL
{

    function __construct(Database $p_cn, $p_id=-1)
    {
        $this->table="public.operation_currency";
        $this->primary_key="id";
        /*
         * List of columns
         */
        $this->name=array(
            "id"=>"id"
            , "oc_amount"=>"oc_amount"
            , "oc_vat_amount"=>"oc_vat_amount"
            , "oc_price_unit"=>"oc_price_unit"
            , "j_id"=>"j_id"
        );
        /*
         * Type of columns
         */
        $this->type=array(
            "id"=>"numeric"
            , "oc_amount"=>"numeric"
            , "oc_vat_amount"=>"numeric"
            , "oc_price_unit"=>"numeric"
            , "j_id"=>"numeric"
        );


        $this->default=array(
            "id"=>"auto"
        );

        $this->date_format="DD.MM.YYYY";
        parent::__construct($p_cn, $p_id);
    }

}
