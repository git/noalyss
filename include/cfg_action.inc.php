<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Copyright Author Dany De Bontridder danydb@aevalys.eu

/*!\file
 * \brief this file is included to perform modification on category of document (= action)
 * table document_type
 */

// show list of document
if (!defined('ALLOWED'))     die('Appel direct ne sont pas permis');
echo '<div class="content">';
$doc_type=new Document_type_SQL($cn);
$action_document_type=new Action_Document_Type_MTable($doc_type);

$action_document_type->set_callback("ajax_misc.php");
$action_document_type->add_json_param("op", "cfgaction");

$action_document_type->create_js_script();
$action_document_type->display_table("order by dt_value");
?>
</div>


 