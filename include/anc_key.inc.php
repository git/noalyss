<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS isfree software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS isdistributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright 2014 Author  Dany De Bontridder ddebontridder@yahoo.fr

/**
 * @file
 * @brief  manage distribution keys for Analytic accountancy, this file is called by 
 * do.php
 * @see do.php
 * 
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
global $cn, $g_user;
global $http;

$op=$http->request("op", "string", "list");
switch ($op)
{
    case 'list':
        Anc_Key::display_list();
        Anc_Key::key_add();
        break;
    case 'consult':
        try
        {
            $id=$http->request("key", "number");
            $key=new Anc_Key($id);
            if (isset($_POST['save_key']))
            {
                $key->save($_POST);
                Anc_Key::display_list();
                Anc_Key::key_add();

                break;
            }
            $key->input();
        }
        catch (Exception $e)
        {
              record_log($e);
            echo span($e->getMessage(), ' class="warning"');
            Anc_Key::display_list();
            Anc_Key::key_add();
        }
        break;
    case 'delete_key':
        try
        {
            $id=$http->request("key", "number");
            $key=new Anc_Key($id);
            $key->delete();
        }
        catch (Exception $e)
        {
            echo span($e->getMessage(), ' class="warning"');
        }
        Anc_Key::display_list();
        Anc_Key::key_add();
}
?>
