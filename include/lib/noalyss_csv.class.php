<?php

/*
 * Copyright (C) 2015 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


/**
 * @file 
 * @brief Manage the CSV : manage files and write CSV record
 *
 */


/**
 * @brief Manage the CSV : manage files and write CSV record
 *
 */
class Noalyss_Csv
{

    private $filename;
    private $element;
    private $sep_field;
    private $sep_dec;
    private $encoding;
    private $size;

    function __construct($p_filename)
    {
        $this->filename=$p_filename;
        $this->element=array();
        $this->size=0;
     
        $a_field=[';',','];
        $this->sep_field=$a_field[$_SESSION[SESSION_KEY.'csv_fieldsep']];
        $a_field=['.',','];
        $this->sep_dec=$a_field[$_SESSION[SESSION_KEY.'csv_decimal']];
        $this->encoding=$_SESSION[SESSION_KEY.'csv_encoding'];
    
    }

    /***
     * @brief
     * Correct the name of the file , remove forbidden character and
     * add extension and date
     */
    protected  function correct_name()
    {
        if (trim(strlen($this->filename))==0) {
            record_log('CSV->correct_name filename is empty');
            throw new Exception('CSV-CORRECT_NAME');
        }
        $this->filename.="-".date("ymd-Hi");
        $this->filename.=".csv";
        
        $this->filename=noalyss_str_replace(";", "", $this->filename);
        $this->filename=noalyss_str_replace("/", "", $this->filename);
        $this->filename=noalyss_str_replace(":", "", $this->filename);
        $this->filename=noalyss_str_replace("*", "", $this->filename);
        $this->filename=noalyss_str_replace(" ", "_", $this->filename);
        $this->filename=strtolower($this->filename);
    }

    /***
     *@brief  Send an header for CSV , the filename is corrected
     */
    function send_header()
    {
        $this->correct_name();
        header('Pragma: public');
        header('Content-type: application/csv');
        header("Content-Disposition: attachment;filename=\"{$this->filename}\"",
                FALSE);
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Expires: Sun, 1 Jan 2000 12:00:00 GMT');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').'GMT');
    }

    /***
     * @brief write header
     * @param array $p_array Array of 1 dimension with the contains of a row
     * 
     */
    function write_header($p_array)
    {
        $size_array=count($p_array);
        $sep="";
        for ($i=0; $i<$size_array; $i++)
        {
            
            printf($sep.'"%s"', $this->encode($p_array[$i]));
            $sep=$this->sep_field;
        }
        printf("\r\n");
    }
    /***
     * @brief Add column to export to csv , the string are enclosed with
     * double-quote,
     * @param $p_item value to export
     * @param $p_type must be text(default) or number
     * @throws Exception
     */
    function add($p_item,$p_type="text") 
    {
        if ( ! in_array($p_type, array("text","number"))) {
                throw new Exception("NOALYSS_CSV::ADD");
        }
        $this->element[$this->size]['value']=$p_item;
        $this->element[$this->size]['type']=$p_type;
        $this->size++;
    }
    /***
     * @brief the string are enclosed with  double-quote,
     *  we remove problematic character and
     * the number are formatted.
     * Clean the row after exporting
     * @return nothing
     */
    function write() 
    {
        if ($this->size == 0 ) return;
        $sep="";
        for ($i=0;$i < $this->size;$i++)
        {
            if ($this->element[$i]['type'] == 'number' )
            {
                printf($sep.'%s', $this->nb($this->element[$i]['value']));
            }
            else
            {
                $export=($this->element[$i]['value']==null)?"":$this->element[$i]['value'];
                // remove break-line, 
                $export=noalyss_str_replace("\n"," ",$export);
                $export=noalyss_str_replace("\r"," ", $export);
                // remove double quote
                $export=noalyss_str_replace('"',"", $export);
                printf($sep.'"%s"', $this->encode($export));
            }
            $sep=$this->sep_field;
        }
        printf("\r\n");
        $this->clean();
    }
    /**
     * clean the row
     */
    protected function clean()
    {
        $this->element=array();
        $this->size=0;
    }
    /**
    * format the number for the CSV export
    * @param $p_number number
    */
    protected function nb($p_number)
   {
       $p_number=noalyss_trim($p_number);
       if ($p_number=="") {return $p_number;}
       if ( isNumber($p_number) == 1 ) {
            $r=number_format($p_number, 4, $this->sep_dec,'');
       } else {
           $r=$p_number;
       }
       return $r;
   }
    protected function encode($str)
   {
       if ($this->encoding=="utf8") return $str;
       if ($this->encoding=="latin1") return mb_convert_encoding($str,'ISO-8859-1','UTF-8');
       throw new Exception(_("Encodage invalide"));
   }
    /**
     * @return mixed
     */
    public function get_filename()
    {
        return $this->filename;
        return $this;
    }

    /**
     * @param mixed $filename
     */
    public function set_filename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return array
     */
    public function get_element()
    {
        return $this->element;
        return $this;
    }

    /**
     * @param array $element
     */
    public function set_element($element)
    {
        $this->element = $element;
        return $this;
    }

    /**
     * @return mixed
     */
    public function get_sep_field()
    {
        return $this->sep_field;
        return $this;
    }

    /**
     * @param mixed $sep_field
     */
    public function set_sep_field($sep_field)
    {
        $this->sep_field = $sep_field;
        return $this;
    }

    /**
     * @return mixed
     */
    public function get_sep_dec()
    {
        return $this->sep_dec;
        return $this;
    }

    /**
     * @param mixed $sep_dec
     */
    public function set_sep_dec($sep_dec)
    {
        $this->sep_dec = $sep_dec;
        return $this;
    }

    /**
     * @return mixed
     */
    public function get_encoding()
    {
        return $this->encoding;
        return $this;
    }

    /**
     * @param mixed $encoding
     */
    public function set_encoding($encoding)
    {
        $this->encoding = $encoding;
        return $this;
    }

    /**
     * @return int
     */
    public function get_size()
    {
        return $this->size;
        return $this;
    }

    /**
     * @param int $size
     */
    public function set_size($size)
    {
        $this->size = $size;
        return $this;
    }

}
